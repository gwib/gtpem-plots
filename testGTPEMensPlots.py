#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 11:52:59 2022

@author: GalinaJonat
"""
from plotGTPEMens_nc import *

#%% let's go!
# processing metadata
meta_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220608/folder_manifest.csv'
sim_metadf = read_parametersFromManifest(meta_fp)
simTerrainDict = buildSimParamDict(sim_metadf)

# processing netCDF
nc_fp  = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220608/result_soil_temperature.nc'
kdi_geotop_Tg = xr.open_dataset(nc_fp,group='geotop')

# select example site
test_site = kdi_geotop_Tg.sitename.values[0]

test_kdi_Ds = kdi_geotop_Tg.where((kdi_geotop_Tg.sitename == test_site),drop=True).drop('pointid')

#%% Plot time series
siteTimeSeriesPerDepth(kdi_geotop_Tg, test_site, simTerrainDict, 'Tg', 'Ground temperature [C]')

nc_ice_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220608/result_ice_content.nc'
kdi_geotop_ice = xr.open_dataset(nc_ice_fp,group='geotop')

siteTimeSeriesPerDepth(kdi_geotop_ice, test_site, simTerrainDict, 'ice_content', 'Ice content')

nc_water_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220608/result_liquid_water.nc'
kdi_geotop_liq_water = xr.open_dataset(nc_water_fp,group='geotop')

siteTimeSeriesPerDepth(kdi_geotop_liq_water, test_site, simTerrainDict, 'liq_water', 'Liquid water content')

#%% Plot trumpet curves

kdi_ensDfDict = createEnsDfDictPerSite(kdi_geotop_Tg,test_site,2013)

kdi_trumpetEnsDf, sims = trumpetCurveEnsDf(kdi_ensDfDict)

#trumpetPlotEnsemble(kdi_trumpetEnsDf, simTerrainDict, sims)

trumpetPlotEnsemblePerTerrain(kdi_trumpetEnsDf, simTerrainDict, sims)
