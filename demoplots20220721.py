#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 10:05:34 2022

@author: GalinaJonat
"""
# DEMO PLOTS
#%%imports
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt

#%% anomaly (Tg)

#%% time series for variable
from timeSeriesEns import timeSeriesForSite, timeSeriesForSitePerDepth
from sys import platform
folder_talik = '/home/gjo713/storage/gtpem_output/'
folder_os = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/'
sim_folder = 'kdi_ens_20220728/'
liq_water_fn = 'result_liquid_water.nc'
ice_content_fn = 'result_ice_content.nc'
#soil_temp_fn = 'result_soil_temperature.nc'

if platform=='linux':
    folder = folder_talik
elif platform=='darwin':
    folder = folder_os
#Tg
tg_annual_fp = folder+sim_folder+'resampled/soil_temperature_annualMean.nc'
tg_annual = xr.open_dataset(tg_annual_fp)
test_site = tg_annual.sitename.values[0,0]


test_kdi_tg = tg_annual.where((tg_annual.sitename == test_site),drop=True).drop('pointid')

timeSeriesForSitePerDepth(test_kdi_tg, var='Tg', qt_values=(0.1,0.9))

# %% Ice content and TDD

iceContent_min_ds = folder+sim_folder+'/resampled/ice_content_annualMin_integrated.nc'
#tdd =

