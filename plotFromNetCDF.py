#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 15:50:27 2022

@author: GalinaJonat
"""

import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import pdb
import pickle
import pandas as pd
from datetime import date, timedelta

def readDataset(fp,groups=[]):
    ds = nc.Dataset(fp)
    groupDs = {}
    if len(groups) > 0:
        for g in groups:
            groupDs[g] = ds.createGroup(g)
    return ds, groupDs

def timeToDate(time):
    dates = []
    start = date(1,1,1)
    for t in time:
        days = int(t)
        delta = timedelta(days)
        offset = start + delta
        dates.append(offset)
    return dates

def outputPerSim(groupDs, sims,var='Tg'):
    #pdb.set_trace()
    if type(sims[0]) == np.ndarray:
        sims = list(sims[:])
    outputPerSim={}
    i=0
    for s in sims:
        outputPerSim[s] = np.array(groupDs[var])[i,:,:]
        i+=1
    return outputPerSim
    
def plotOutputPerSim(outputPerSim, sims,depths,terrain='',yvar='Temperature [C]',dates=[]):
    if type(sims[0]) == np.ndarray:
        sims = list(sims[:])
    figs = {}
    for s in sims:
        if len(terrain) > 1:
            title = 'Simulation: '+s + '\n Terrain: '+ terrain
        else:
            title =  'Simulation: '+s
        plt.figure()
        plt.title(title)
        if len(dates) > 1:
            for d in range(len(depths)):
                plt.plot(dates,outputPerSim[s][:,d],label=str(depths[d])+ " m")
        else:
            for d in range(len(depths)):
                plt.plot(outputPerSim[s][:,d],label=str(depths[d])+ " m")
        plt.ylabel(yvar)
        plt.xlabel('Date')
        plt.legend()
        plt.show()


def plotOutputPerSite(tgPerSim, sims,depths,dates,manifest_fp='',yvar='Temperature [C]'):
    
    
    if len(manifest_fp) > 1:
        metadata_df = read_parametersFromManifest(manifest_fp)
    
    if type(sims[0]) == np.ndarray:
        sims = list(sims[:])
    
    for s in sims:
        try:
            pars = list(metadata_df.loc[[s]].parameters)[0]
        except:
            pars = ''
        
    plotOutputPerSim(tgPerSim, sims, depths,terrain=pars,yvar=yvar,dates=dates)
        
        #plt.show()

def plotOutputPerSiteSubplots(groupDs, sims,depths,dates):
    simOutput = outputPerSim(groupDs, sims)
    if type(sims[0]) == np.ndarray:
        sims = list(sims[:])
    
    fig, axs = plt.subplots(2,2)

def inptsFpToParameter(inpts_fp_str):
    
    if ';' in inpts_fp_str:
        parameters = ''
        splits = inpts_fp_str.split(';')
        for s in splits:
            parameters += s.split('/')[-1].split('.')[-2]
    else:        
        parameters = inpts_fp_str.split('/')[-1].split('.')[-2]
    return parameters


def read_parametersFromManifest(manifest_fp):
    sim_metadata = pd.read_csv(manifest_fp)
    del sim_metadata['id']
    sim_metadata.set_index('directory', inplace=True)
    sim_metadata['parameters'] = sim_metadata['parameters'].apply(lambda x: inptsFpToParameter(x))
    #events['start_time'] = events['start_time'].apply(lambda x: dt.datetime.strptime(x, "%Y-%m-%d %H:%M"))
    return sim_metadata
    
# =============================================================================
# fp_eskerBedrock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/result_soil_temperature_eskerBedrock.nc'
# ds_eskerBedrock, group_eskerBedrock = readDataset(fp_eskerBedrock,groups=['geotop'])
# geotop_eskerBedrock = group_eskerBedrock['geotop']
# sims_eskerBedrock = geotop_eskerBedrock['simulation'][:]
# outSim_eskerBedrock = outputPerSim(geotop_eskerBedrock,sims_eskerBedrock)
# plotSims = ['geotop_NGODD10_scaled_merr_481c047','geotop_NGODD10_scaled_merr_f165a6d', 'geotop_NGODD10_scaled_era5_0bc5d37', 'geotop_NGODD10_scaled_era5_9018b33']
# =============================================================================

