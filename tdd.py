#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to calculate thaw depth duration

Created on Tue Jul  5 13:18:28 2022

@author: GalinaJonat
"""
#%% imports
import xarray as xr
import numpy as np
#from timeSeriesEns import timeSeriesForSite

#%% functions
def H_xr(T):
    func = lambda x: np.heaviside(x,0)
    return xr.apply_ufunc(func,T)


def dz(z):
    no_depths = len(z)
    dz = np.zeros(no_depths)
    i = 0
    for i in range(no_depths):
        if i == 0:
            dz[i] = z[0]
        elif i > 0:
            dz[i] = z[i] - z[i-1]
    return dz

# %% calculate thaw depth duration from daily mean and include sitename in final file
def calcTDD(tg_dataset,nc_fp):
    z = tg_dataset.soil_depth.values
    deltaz = dz(z)

    kdi_Tg_heaviside = tg_dataset.assign(H_zt=lambda x: np.heaviside(tg_dataset['Tg'],0))

    kdi_heaviside = kdi_Tg_heaviside.drop_vars(['Tg'])

    kdi_heaviside_annual_t = kdi_heaviside.resample(Date='Y').sum(dim='Date')
    kdi_heaviside_annual_zt = (kdi_heaviside_annual_t['H_zt'] * deltaz).values
    kdi_heaviside_integrated = kdi_heaviside_annual_zt['H_zt'].sum('soil_depth')

    # dates do not align between kdi_sites and kdi_heaviside_integrated, need to extract year and then merge
    years = kdi_heaviside_integrated.Date.values.astype('datetime64[Y]').astype(int) + 1970
    sims = kdi_heaviside_integrated.simulation.values



    siteArray = xr.Dataset(
        data_vars=dict(
            sitename=(['Date','simulation'], np.empty((len(years), len(sims)),dtype='a24'))
        ),
        coords=dict(
            Date=years,
            simulation=sims
        ),
    )

    for year in years:
        date = str(year)+'-12-31T00:00:00.000000000'
        for sim in sims:
            simDateDict = {'Date': date, 'simulation': sim}
            site = str(kdi_Tg_heaviside.sel(simDateDict).sitename.values)
            simYearDict = {'Date': year, 'simulation': sim}
            siteArray.loc[(simYearDict)] = site

    tdd = kdi_heaviside_integrated.H_zt.values
    tdd_integrated2d = xr.Dataset(
        data_vars=dict(
            tdd=(['Date','simulation'], tdd)
        ),
        coords=dict(
            Date=years,
            simulation=sims
        ),
        attrs=dict(
            description=f'Thaw depth duration, integrated over [0,{np.max(z)}] m'
        ),
    )
    siteToStr = np.vectorize(decode)
    sites = siteArray.sitename.values
    sitesStr = siteToStr(sites)

    siteArray2 = xr.Dataset(
        data_vars=dict(
            sitename=(['Date','simulation'], sitesStr)
        ),
        coords=dict(
            Date=years,
            simulation=sims
        ),
    )

    tdd_incl_sitename = tdd_integrated2d.merge(siteArray2)
    tdd_incl_sitename.to_netcdf(nc_fp+'/resampled/tdd.nc')


# %%
def decode(s):
    return s.decode("utf-8")

#%%
def generateTdd_ds(daily_tg_ds):
    z = daily_tg_ds.soil_depth.values
    deltaz = dz(z)

    kdi_Tg_heaviside = daily_tg_ds.assign(H_zt=lambda x: np.heaviside(daily_tg_ds['Tg'],0))

    kdi_heaviside = kdi_Tg_heaviside.drop_vars(['Tg'])

    kdi_heaviside_annual_t = kdi_heaviside.resample(Date='Y').sum(dim='Date')
    kdi_heaviside_annual_zt = kdi_heaviside_annual_t['H_zt'] * deltaz
    tdd_annual_z = kdi_heaviside_annual_zt.values
    tdd = np.empty(tdd_annual_z.shape[:-1]) * np.nan
    for d in range(0,tdd_annual_z.shape[0]):
        for s in range(0,tdd_annual_z.shape[1]):
            tdd[d,s] = np.sum(tdd_annual_z[d,s,:])
    years = [int(np.datetime_as_string(d,unit='Y')) for d in kdi_heaviside_annual_t.Date.values]
    sims = kdi_heaviside_annual_t.simulation.values
    tdd_ds = xr.Dataset(
        data_vars=dict(
                tdd=(['Date','simulation'], tdd)
            ),
            coords=dict(
                Date=years,
                simulation=sims
            ),
            attrs=dict(
                description=f'Thaw depth duration, integrated over [0,{np.max(z)}] m'
            ),
        )
    return tdd_ds
