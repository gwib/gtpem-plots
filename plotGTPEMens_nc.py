import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import numpy as np

#%% functions to read file manifest
def inptsFpToParameter(inpts_fp_str):
    
    if ';' in inpts_fp_str:
        parameters = ''
        splits = inpts_fp_str.split(';')
        for s in splits:
            parameters += s.split('/')[-1].split('.')[-2]
    else:        
        parameters = inpts_fp_str.split('/')[-1].split('.')[-2]
    return parameters

def read_parametersFromManifest(manifest_fp):
    sim_metadata = pd.read_csv(manifest_fp)
    del sim_metadata['id']
    sim_metadata['parameters'] = sim_metadata['parameters'].apply(lambda x: inptsFpToParameter(x))
    return sim_metadata

def buildSimParamDict(sim_metadf):
    simParamDict = {}
    for s in sim_metadf['directory']:
        simParamDict[s] = sim_metadf.loc[sim_metadf['directory'] == s].parameters.values[0]
    return simParamDict

#%% functions to process GTPEM output netCDF file
def dsPerSite(ensDs,site):
    siteDs = ensDs.where(ensDs.sitename == site,drop=True)
    return siteDs

def createEnsDfDictPerSite(ensDs,site,year):
    site_Ds = ensDs.where((ensDs.sitename == site) & (year==2013),drop=True).drop('pointid')
    simEnsDfDict = {}
    siteSims = site_Ds.simulation.values
    
    for s in siteSims:
        simEnsDfDict[s] = site_Ds.sel(simulation=s)
        
    return simEnsDfDict
#%% Helper functions
forcingLine = {'ERA5': ':', 'JRA': '-.', 'MERRA':'--'} 

def simIdToForcing(simName):
    if  'merr' in simName:
        return 'MERRA'
    if 'jra' in simName:
        return 'JRA'
    if 'era' in simName:
        return 'ERA5'


#%% PLOTS
#%%% plot site time series per depth
def siteTimeSeriesPerDepth(ensDs, site, simTerrainDict, metric, metricName):
    site_Ds = ensDs.where(ensDs.sitename == site,drop=True)
    depthDsDict = {}
    depths = list(site_Ds.soil_depth.values)
    no_depths = len(depths)
    
    position = range(1,no_depths+1)
    
    fig = plt.figure()
    for d in depths:
        pos = depths.index(d) + 1
        depthDs = site_Ds.sel(soil_depth=d)
        ax = fig.add_subplot(no_depths,1,pos)
        for s in depthDs.simulation.values:
            forcing = simIdToForcing(s)
            parameter = simTerrainDict[s]
            simDf = depthDs.sel(simulation=s).to_dataframe()
            ax.plot(simDf['Date'],simDf[metric],label=parameter+' '+forcing)
        
        ax.set_title(f'Ensemble plot at site {site} depth {d} m')
        ax.set_ylabel(metricName)
        
    ax.set_xlabel('Date')
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper center')
        
def timeSeriesPerTerrain(ensDs,site,simTerrainDict,metric,metricName):
    terrainTypes = []

    site_terrainTypes = np.unique(list(simTerrainDict.values()))
    sims = ensDs.simulation.values
    # Create a Position index
    
    noTerrainTypes = len(site_terrainTypes)
    
    position = range(1, noTerrainTypes + 1)
        
    fig = plt.figure()
    
    subplotForTerrain = {}
    for k in range(len(sims)):
        s = sims[k]
        forcing = simIdToForcing(s)
        parameter = simTerrainDict[s]

                

        print(f'Forcing {forcing}; terrain {parameter}')
        
        if (parameter not in subplotForTerrain.keys()):
            if k == 0:
                ax1 = fig.add_subplot(noTerrainTypes,1,position[k])
                subplotForTerrain[parameter] = ax1
            elif k >= 1:
                subplotForTerrain[parameter] = fig.add_subplot(noTerrainTypes,1,position[k],sharex=ax1)
        
        #subplotForTerrain[parameter].plot(max_T,-depth,c='red',linestyle=forcingLine[forcing])
        subplotForTerrain[parameter].legend()
        subplotForTerrain[parameter].set_title(f'{parameter}',y=1.0,pad=-14)
        subplotForTerrain[parameter].set_ylabel('Depth [m]')
        
#%%% trumpet curves

# ds per site
# df per sim
# df groupby('depth') -- max, min, mean



# plot trumpet curve per site, one plot per sim
def trumpetCurveEnsDf(ensDfDict,metric='Tg'):
    trumpetCurveEns_df = pd.DataFrame()
    simulations = list(ensDfDict.keys())
    
    depth = np.unique(ensDfDict[simulations[0]]['soil_depth'].values)
    
    trumpetCurveEns_df['soil_depth'] = depth
    trumpetCurveEns_df.set_index('soil_depth',inplace=True)
    
    print(f'Trumpet curve dataframe initialised for {len(simulations)} simulations')
    for s in simulations:
        simDf = ensDfDict[s]

        colMin = s+'_min'
        colMax = s+'_max'
        colMean = s+'_mean'
        
        min_values = simDf.groupby('soil_depth').min().get(metric).values
        max_values = simDf.groupby('soil_depth').max().get(metric).values
        mean_values = simDf.groupby('soil_depth').mean().get(metric).values
        
        
        trumpetCurveEns_df[colMin] = min_values
        trumpetCurveEns_df[colMax] = max_values
        trumpetCurveEns_df[colMean] = mean_values
    
    return trumpetCurveEns_df, simulations

def trumpetPlotEnsemble(trumpet_df,simTerrainDict,sims):

    no_trumpets = len(sims)
    
    # Create a Position index
    
    position = range(1,no_trumpets + 1)
    
    depth = trumpet_df.index.values
    
    fig = plt.figure()
    for k in range(no_trumpets):
        s = sims[k]
        forcing = simIdToForcing(s)
        parameter = simTerrainDict[s]
        
        max_T = trumpet_df[s+'_max'].values
        min_T = trumpet_df[s+'_min'].values
        mean_T = trumpet_df[s+'_mean'].values
        
        # add every single subplot to the figure with a for loop
        if k == 0:
            ax1 = fig.add_subplot(no_trumpets,1,position[k])
            ax = ax1
        elif k >= 1:
            ax = fig.add_subplot(no_trumpets,1,position[k],sharex=ax1)
        ax.plot(max_T,-depth,c='red')
        ax.plot(min_T,-depth,c='blue')
        ax.plot(mean_T,-depth,c='gray')
        ax.set_title(f'{parameter} {forcing}',y=1.0,pad=-14)
        ax.set_ylabel('Depth [m]')
    
    ax.set_xlabel('Temperature [C]')
    
    plt.show()    



#TODO: plot trumpet curve per site, one subplot per terrain type
def trumpetPlotEnsemblePerTerrain(trumpet_df,simTerrainDict,sims):
    
    site_terrainTypes = np.unique(list(simTerrainDict.values()))
    no_trumpets = len(site_terrainTypes)
    
    # Create a Position index
    
    position = range(1,no_trumpets + 1)
    
    depth = trumpet_df.index.values
    
    fig = plt.figure()
    subplotForTerrain = {}
    for k in range(len(sims)):
        s = sims[k]
        forcing = simIdToForcing(s)
        parameter = simTerrainDict[s]
        
        max_T = trumpet_df[s+'_max'].values
        min_T = trumpet_df[s+'_min'].values
        mean_T = trumpet_df[s+'_mean'].values
        
        print(f'Forcing {forcing}; terrain {parameter}')
        
        if (parameter not in subplotForTerrain.keys()):
            if k == 0:
                ax1 = fig.add_subplot(no_trumpets,1,position[k])
                subplotForTerrain[parameter] = ax1
            elif k >= 1:
                subplotForTerrain[parameter] = fig.add_subplot(no_trumpets,1,position[k],sharex=ax1)
        
        subplotForTerrain[parameter].plot(max_T,-depth,c='red',linestyle=forcingLine[forcing])
        subplotForTerrain[parameter].plot(min_T,-depth,c='blue',linestyle=forcingLine[forcing])
        subplotForTerrain[parameter].plot(mean_T,-depth,c='gray',linestyle=forcingLine[forcing],label=forcing)
        subplotForTerrain[parameter].legend()
        subplotForTerrain[parameter].set_title(f'{parameter}',y=1.0,pad=-14)
        subplotForTerrain[parameter].set_ylabel('Depth [m]')
    
    subplotForTerrain[parameter].set_xlabel('Temperature [C]')
    
    plt.show()    
#%contour plot