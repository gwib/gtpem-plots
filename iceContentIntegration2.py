#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 22 15:12:01 2022

@author: GalinaJonat
"""
import xarray as xr
import numpy as np
from scipy import interpolate
from scipy.integrate import quad
from sys import platform
from tdd import siteToStr



folder_talik = '/home/gjo713/storage/gtpem_output/'
folder_os = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/'
sim_folder = 'kdi_ens_20221109/'
iceContent_fn = '/resampled/ice_content_annualMin.nc'

print(sim_folder)
if platform=='linux':
    folder = folder_talik+sim_folder
elif platform=='darwin':
    folder = folder_os+sim_folder

fp_ice = folder+iceContent_fn
iceContentAnnualMin = xr.open_dataset(fp_ice)
iceContent = iceContentAnnualMin.drop_vars(['pointid','model'])

#%% Integrate ice content for all sites
years = iceContent.Date.values
sims = iceContent.simulation.values
z = iceContent.soil_depth.values
sites = np.unique(iceContent.sitename.values)
# create new dataset with dimensions year and simulation
iceContent_integrated2dAll = xr.Dataset(
    data_vars=dict(
        totalIceContent=(['Date','simulation'], np.full((len(years), len(sims)),fill_value=np.nan))
    ),
    coords=dict(
        Date=years,
        simulation=sims
    ),
    attrs=dict(
        description=f'Ice content, integrated over depth [0,{np.max(z)}] m'
    ),
)
#ice_content_simDate = np.full((len(years), len(sims)),fill_value=np.nan)
siteArray = xr.Dataset(
    data_vars=dict(
        sitename=(['Date','simulation'], np.empty((len(years), len(sims)),dtype='a24'))
    ),
    coords=dict(
        Date=years,
        simulation=sims
    ),
)
for year in years:
    date = year
    for sim in sims:
        simDateDict = {'Date': date, 'simulation': sim}
        site = str(iceContent.sel(simDateDict).sitename.values)
        simYearDict = {'Date': year, 'simulation': sim}
        ice_contentSimYear = iceContent.sel(simDateDict).ice_content.values
        f_ice = interpolate.interp1d(z,ice_contentSimYear,fill_value=ice_contentSimYear[0]/2,bounds_error=False)
        iceContent_integrated2dAll.loc[(simYearDict)] = quad(f_ice,0,np.max(z))[0]
        siteArray.loc[(simYearDict)] = site


sites = siteArray.sitename.values
sitesStr = siteToStr(sites)
siteArray2 = xr.Dataset(
    data_vars=dict(
        sitename=(['Date','simulation'], sitesStr)
    ),
    coords=dict(
        Date=years,
        simulation=sims
    ),
)


iceContent_integrated_sitename = iceContent_integrated2dAll.merge(siteArray2)
iceContent_integrated_sitename.to_netcdf(folder+'/resampled/ice_content_annualMin_integrated.nc')
