#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 18:08:07 2022

@author: GalinaJonat
"""
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt

#%% functions
def timeSeriesForSitePerDepth(site_ds,var='Tg', qt_values=(0.1,0.9),ylabel='Ground temperature [C]'):
    
    ds_tg = site_ds.drop_vars(['model','sitename'])
    
    ds_min = ds_tg.min(dim='simulation')
    ds_max = ds_tg.max(dim='simulation')
    ds_mean = ds_tg.mean(dim='simulation')

    ds_quantiles = ds_tg.quantile(qt_values,dim='simulation')

    
    depthsDsDict = {}
    depths = list(ds_tg.soil_depth.values)
    no_depths = len(depths)
    
    position = range(1,no_depths+1)
    
    date = np.array(ds_tg.Date.values)
    
    fig = plt.figure()
    for d in depths:
        pos = depths.index(d)+1
        
        mean = np.array(ds_mean.sel(soil_depth = d)[var].values)
        lowerquantile = np.array(ds_quantiles.sel(soil_depth = d, quantile = qt_values[0])[var].values)
        upperQuantile = np.array(ds_quantiles.sel(soil_depth = d, quantile = qt_values[1])[var].values)
        
        ax = fig.add_subplot(no_depths,1,pos)
        ax.plot(date,mean,label='Ensemble mean')    
        ax.fill_between(date,lowerquantile,upperQuantile,alpha=0.5,label=f'{qt_values} ensemble confidence interval')
        
        ax.set_title(f'Ensemble time series at depth {d} m')
        ax.set_ylabel(ylabel)
    
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper center')    
    fig.tight_layout()
        
def timeSeriesForSite(site_ds_orig, var,qt_values=(0.1,0.9),ylabel='Ground temperature [C]'):
    try:
        site_ds = site_ds_orig.drop_vars(['model','sitename'])
    except:
        site_ds = site_ds_orig
        print('Variables [sitename, model] do not need to be dropped.')
    
    print(site_ds.data_vars)
    
    ds_min = site_ds.min(dim='simulation')
    ds_max = site_ds.max(dim='simulation')
    ds_mean = site_ds.mean(dim='simulation')

    ds_quantiles = site_ds.quantile(q=qt_values,dim='simulation')
        #position = range(1,no_depths+1)
    try:    
        date = np.array(site_ds['Date.year'].values)
    except:
        date = site_ds['Date'].values
       
    fig = plt.figure()
    mean = ds_mean[var].values
    lowerquantile = np.array(ds_quantiles.sel(quantile = qt_values[0])[var].values)
    upperQuantile = np.array(ds_quantiles.sel(quantile = qt_values[1])[var].values)

    print(lowerquantile)
    print(upperQuantile)

    ax = fig.add_subplot()
    ax.plot(date,mean,label='Ensemble mean')    
    ax.fill_between(date,lowerquantile,upperQuantile,alpha=0.5,label=f'{qt_values} quantiles')
            
    ax.set_title(f'Ensemble time series')
    ax.set_ylabel(ylabel)
        
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper center')    
    fig.tight_layout()