import xarray as xr
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from sys import platform
sns.set_theme(style='darkgrid')

folder_talik = '/home/gjo713/storage/gtpem_output/'
folder_os = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/'

if platform=='linux':
    folder = folder_talik
elif platform=='darwin':
    folder = folder_os

forcing_ERA5_Ds = xr.open_dataset(folder+'/kdi_forcing/scaled_era5_1h_scf1_kdiBoreholes.nc')
# select station
era5_site = forcing_ERA5_Ds.where((forcing_ERA5_Ds.station == 68),drop=True).drop_vars('station')
# monthly mean
era5_site_monthly = era5_site.resample(time='3M').mean(dim='time')

era5_T2m_monthly_df = era5_site_monthly.to_dataframe()
era5_T2m_monthly_df.drop(index=era5_T2m_monthly_df.index[0], axis=0, inplace=True)

figERA5, axERA5 = plt.subplots()

sns.lineplot(data=era5_T2m_monthly_df,x='time',y='AIRT_sur',ax=axERA5)
axERA5.set_ylabel('2m Air Temperature [C]')
axERA5.set_title('ERA5')
axERA5.set_xlabel('Year')
axERA5.set_ylim([-28,12])
figERA5.savefig('/home/gjo713/storage/plots/T2m_ERA5.png')


forcing_merra_Ds = xr.open_dataset(folder+'/kdi_forcing/scaled_merra2_1h_scf1_kdiBoreholes.nc')
merra_site = forcing_merra_Ds.where((forcing_merra_Ds.station == 68),drop=True).drop_vars('station')
merra_site_monthly = merra_site.resample(time='3M').mean(dim='time')

merra_T2m_monthly_df = merra_site_monthly.to_dataframe()
merra_T2m_monthly_df.drop(index=merra_T2m_monthly_df.index[0], axis=0, inplace=True)

forcing_jra_Ds = xr.open_dataset(folder+'/kdi_forcing/scaled_jra55_1h_scf1_KDI_boreholes.nc')
jra_site = forcing_jra_Ds.where((forcing_jra_Ds.station == 68),drop=True).drop_vars('station')
jra_site_monthly = jra_site.resample(time='3M').mean(dim='time')

jra_T2m_monthly_df = jra_site_monthly.to_dataframe()
jra_T2m_monthly_df.drop(index=jra_T2m_monthly_df.index[0], axis=0, inplace=True)

figMERRA, axMERRA = plt.subplots()

sns.lineplot(data=merra_T2m_monthly_df,x='time',y='AIRT_sur',ax=axMERRA)
axMERRA.set_ylabel('2m Air Temperature [C]')
axMERRA.set_title('MERRA')
axMERRA.set_ylim([-28,12])
axMERRA.set_xlabel('Year')
figMERRA.savefig('/home/gjo713/storage/plots/T2m_MERRA.png')

figJRA, axJRA = plt.subplots()
sns.lineplot(data=jra_T2m_monthly_df,x='time',y='AIRT_sur',ax=axJRA)
axJRA.set_ylim([-28,12])
axJRA.set_ylabel('2m Air Temperature [C]')
axJRA.set_title('JRA')
axJRA.set_xlabel('Year')
figJRA.savefig('/home/gjo713/storage/plots/T2m_JRA.png')
