import xarray as xr
import pandas as pd
from ensemble_resample import *

folder = '/home/gjo713/storage/climate_models/ldg/allDisagg_20231106/'

ice_content_fn = 'result_ice_content.nc'
soil_temp_fn = 'result_soil_temperature.nc'
thaw_depth_fn = 'result_thaw_depth.nc'

#%% pre-processing
# filter until 2005
def filterDfByDate(folder,fn):
    data = xr.open_dataset(folder+fn,group='geotop')

    desired_date = pd.Timestamp('2005-01-01')
    filtered_data = data.where(data['Date'] < desired_date, drop=True)
    filtered_data.to_netcdf(folder+'filtered_'+fn)

filterDfByDate(folder,ice_content_fn)
filterDfByDate(folder,soil_temp_fn)
filterDfByDate(folder,thaw_depth_fn)
#%%
kdi_Tg_Ds = xr.open_dataset(folder+'filtered_'+soil_temp_fn, engine='netcdf4')

tg_daily = timeResampleFromGTPEM(kdi_Tg_Ds,'1D',how='mean',nc_path=folder+'/resampled/soil_temperature_dailyMean.nc')
tg_monthly = timeResampleFromGTPEM(kdi_Tg_Ds,'M',how='mean',nc_path=folder+'/resampled/soil_temperature_monthlyMean.nc')
tg_annual = timeResampleFromGTPEM(kdi_Tg_Ds,'Y',how='mean',nc_path=folder+'/resampled/soil_temperature_annualMean.nc')

kdi_ice_Ds = xr.open_dataset(folder+'filtered_'+ice_content_fn, engine='netcdf4')
ice_daily = timeResampleFromGTPEM(kdi_ice_Ds, '1D',nc_path=folder+'/resampled/ice_content_dailyMean.nc')
print('daily done')
ice_monthly = timeResampleFromGTPEM(kdi_ice_Ds, 'M',how='min',nc_path=folder+'/resampled/ice_content_monthlyMin.nc')
print('monthly done')
ice_annual = timeResampleFromGTPEM(kdi_ice_Ds, 'Y',how='min',nc_path=folder+'/resampled/ice_content_annualMin.nc')

thaw_depth_Ds = xr.open_dataset(folder+'filtered_'+thaw_depth_fn, engine='netcdf4')
alt_Ds = timeResampleFromGTPEM(thaw_depth_Ds,timeInterval='Y',how='max',nc_path=folder+'/resampled/result_ALT2.nc')

#%%
import tdd
tg_daily = xr.open_dataset(folder+'/resampled/soil_temperature_dailyMean.nc')

tdd_ds = tdd.generateTdd_ds(tg_daily)

tdd_ds.to_netcdf('/home/gjo713/storage/climate_models/ldg/allDisagg_20231106/resampled/tdd.nc')
# %%
