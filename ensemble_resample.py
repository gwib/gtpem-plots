#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 17:42:54 2022

@author: GalinaJonat

-------------------------
This script takes in GTPEM output files and summarizes the data over a time period with the chosen value (max, min, mean).

"""

#from plotGTPEMens_nc import *
#import matplotlib.pyplot as plt
from time import time
import numpy as np
import xarray as xr
from sys import platform

#%% ACTION -- function
# mean for temperature, min for minimum annual ice content, min for (annual) alt
def timeResampleFromGTPEM(origDs, timeInterval,how='mean',nc_path=''):
    try:
        origDs_Date = origDs.swap_dims({'time': 'Date'})
    except:
        print('Dimensions already swapped.')
        origDs_Date = origDs
    if how=='mean':
        print('resampling to mean')
        resampledDs = origDs_Date.resample(Date=timeInterval).mean(dim='Date')
    elif how=='min':
        resampledDs = origDs_Date.resample(Date=timeInterval).min(dim='Date')
    elif how=='max':
        resampledDs = origDs_Date.resample(Date=timeInterval).max(dim='Date')
    
    if timeInterval=='Y':
        resampledDs['Date'] = resampledDs.indexes['Date'].year
        #resampledDsYear = resampledDs.rename({'Date': 'year'})

    if len(nc_path) > 3:
        resampledDs.to_netcdf(nc_path)
    return resampledDs
