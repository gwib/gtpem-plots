#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 15:32:53 2022

@author: GalinaJonat
"""
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt

#%%% Ensemble summaries
#%%%% daily
nc_daily_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220621/resampled/soil_temperature_dailyMean.nc'
kdi_geotop_Tg_daily = xr.open_dataset(nc_daily_fp)

test_site = kdi_geotop_Tg_daily.sitename.values[0,0]

test_kdi_Ds = kdi_geotop_Tg_daily.where((kdi_geotop_Tg_daily.sitename == test_site),drop=True).drop('pointid')

test_kdi_Ds_min = test_kdi_Ds.min(dim='simulation')
test_kdi_Ds_max = test_kdi_Ds.max(dim='simulation')
test_kdi_Ds_mean = test_kdi_Ds.mean(dim='simulation')

test_kdi_Ds_sd = test_kdi_Ds.std(dim='simulation')


x = np.array(test_kdi_Ds.Date.values)
y = -np.array(test_kdi_Ds.soil_depth.values)

tg_sd = np.array(test_kdi_Ds_sd.Tg.values).transpose()
tg_mean = np.array(test_kdi_Ds_mean.Tg.values).transpose()

fig2,ax2 = plt.subplots()

c2 = ax2.contourf(x,y,tg_mean,cmap=plt.cm.PuBu_r)
fig2.colorbar(c2)
ax2.set_title('Daily mean ground temperatures [C]')
ax2.set_ylabel('Depth [m]')
ax2.set_xlabel('Year')


fig3,ax3 = plt.subplots()
c3 = ax3.contourf(x,y,tg_sd,cmap=plt.cm.gray_r)
fig3.colorbar(c3)
ax3.set_title('Ensemble standard deviation')
ax3.set_ylabel('Depth [m]')
ax3.set_xlabel('Year')

#%%%% monthly
nc_monthly_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220621/resampled/tg_monthly.nc'
kdi_geotop_Tg_monthly = xr.open_dataset(nc_monthly_fp)

test_site = kdi_geotop_Tg_monthly.sitename.values[0,0]

test_kdi_monthly = kdi_geotop_Tg_monthly.where((kdi_geotop_Tg_monthly.sitename == test_site),drop=True).drop('pointid')


test_kdi_monthly_mean = test_kdi_monthly.mean(dim='simulation')

test_kdi_monthly_sd = test_kdi_monthly.std(dim='simulation')


x = np.array(test_kdi_monthly.Date.values)
y = -np.array(test_kdi_monthly.soil_depth.values)

tg_sd = np.array(test_kdi_monthly_sd.Tg.values).transpose()
tg_mean = np.array(test_kdi_monthly_mean.Tg.values).transpose()

fig4,ax4 = plt.subplots()
c4 = ax4.contourf(x,y,tg_mean,cmap=plt.cm.PuBu_r)
fig4.colorbar(c4)
ax4.set_title('monthly mean ground temperatures [C]')
ax4.set_ylabel('Depth [m]')
ax4.set_xlabel('Year')

fig5,ax5 = plt.subplots()
c5 = ax5.contourf(x,y,tg_sd,cmap=plt.cm.gray_r)
fig5.colorbar(c5)
ax5.set_title('Ensemble standard deviation')
ax5.set_ylabel('Depth [m]')
ax5.set_xlabel('Year')

#%%%% annual
nc_yearly_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220621/resampled/soil_tg_yearly.nc'
kdi_geotop_Tg_annual = xr.open_dataset(nc_yearly_fp)

test_kdi_annual = kdi_geotop_Tg_annual.where((kdi_geotop_Tg_annual.sitename == test_site),drop=True).drop('pointid')

#test_kdi_annual_min = test_kdi_annual.min(dim='simulation')
#test_kdi_annual_max = test_kdi_annual.max(dim='simulation')
test_kdi_annual_mean = test_kdi_annual.mean(dim='simulation')

test_kdi_annual_sd = test_kdi_annual.std(dim='simulation')


x = np.array(test_kdi_annual.Date.values)
y = -np.array(test_kdi_annual.soil_depth.values)

tg_sd = np.array(test_kdi_annual_sd.Tg.values).transpose()
tg_mean = np.array(test_kdi_annual_mean.Tg.values).transpose()

fig7,ax7 = plt.subplots()
c7 = ax7.contourf(x,y,tg_mean,cmap=plt.cm.PuBu_r)
fig7.colorbar(c7)
ax7.set_title('Annual mean ground temperatures [C]')
ax7.set_ylabel('Depth [m]')
ax7.set_xlabel('Year')

fig6,ax6 = plt.subplots()
c6 = ax6.contourf(x,y,tg_sd,cmap=plt.cm.gray_r)
fig6.colorbar(c6)
ax6.set_title('Ensemble standard deviation')
ax6.set_ylabel('Depth [m]')
ax6.set_xlabel('Year')