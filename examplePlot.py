#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 16:26:02 2022

@author: GalinaJonat
"""
#%% Imports -- necessary for all runs!
from plotFromNetCDF import *

#%% old simulations reading and plotting
fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/rockSim/result_soil_temperature.nc'
mani_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/rockSim/folder_manifest.csv'
ds, groupDsDict = readDataset(fp,['geotop'])

geotopDs = groupDsDict['geotop']

sitenames = list(geotopDs['sitename'][:])
simIDs = list(geotopDs['simulation'][:])
soil_depth = list(geotopDs['soil_depth'][:])
dates = timeToDate(list(geotopDs['Date'][:]))

siteSimDict = {'simID': simIDs, 'site': sitenames}
siteSimDf = pd.DataFrame(siteSimDict)

tgPerSim_geotop = outputPerSim(geotopDs, simIDs)

simPerSite = {}
for s in sitenames:
    simPerSite[s] = list(siteSimDf[siteSimDf['site']==s]['simID'])
    
plotOutputPerSite(tgPerSim_geotop,simPerSite["NGO-DD-2035"],soil_depth,dates,mani_fp)


plotOutputPerSite(tgPerSim_geotop, simPerSite['NGO-DD-2012'], soil_depth,dates)

#%% Gubler terrain types reading and plotting
#%%% Peat
fp_ice_peat = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/maySims/peat/result_ice_content_peat.nc'
fp_tg_peat = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/maySims/peat/result_soil_temperature_peat.nc'
fp_liqWater_peat = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/maySims/peat/result_liquid_water_peat.nc'

mani_fp_peat = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/maySims/peat/folder_manifest_peat.csv'

ds_ice, groupDsDict_ice = readDataset(fp_ice_peat, ['geotop'])
geotopDs_ice = groupDsDict_ice['geotop']

ds_tg, groupDsDict_tg = readDataset(fp_tg_peat, ['geotop'])
geotopDs_tg_peat = groupDsDict_tg['geotop']

ds_lw, groupDsDict_lw = readDataset(fp_liqWater_peat, ['geotop'])
geotopDs_lw_peat = groupDsDict_lw['geotop']

sitenames_peat = list(geotopDs_ice['sitename'][:])
simIDs_peat = list(geotopDs_ice['simulation'][:])
soil_depth_peat = list(geotopDs_ice['soil_depth'][:])
dates_peat = timeToDate(list(geotopDs_ice['Date'][:]))

siteSimDict_peat =  {'simID': simIDs_peat, 'site': sitenames_peat}
siteSimDf_peat = pd.DataFrame(siteSimDict_peat)

iceContentPerSim_geotop_peat = outputPerSim(geotopDs_ice, simIDs_peat,'ice_content')
tgPerSim_geotop_peat = outputPerSim(geotopDs_tg_peat, simIDs_peat,'Tg')
lwPerSim_peat = outputPerSim(geotopDs_lw_peat, simIDs_peat, 'liq_water')

simPerSite_peat = {}
for s in sitenames_peat:
    simPerSite_peat[s] = list(siteSimDf_peat[siteSimDf_peat['site']==s]['simID'])

plotOutputPerSite(iceContentPerSim_geotop_peat, simPerSite_peat['NGO-DD-2012'], soil_depth_peat, dates_peat,manifest_fp=mani_fp_peat,yvar='Ice content')
plotOutputPerSite(tgPerSim_geotop_peat, simPerSite_peat['NGO-DD-2012'], soil_depth_peat, dates_peat,yvar='Ground Temperature [C]',manifest_fp=mani_fp_peat)
plotOutputPerSite(lwPerSim_peat,simPerSite_peat['NGO-DD-2012'], soil_depth_peat, dates_peat,yvar='Liquid water content',manifest_fp=mani_fp_peat)

#%%% Rock
fp_ice_rock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/maySims/rock/result_ice_content.nc'
fp_tg_rock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/maySims/rock/result_soil_temperature.nc'
fp_liqWater_rock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/maySims/rock/result_liquid_water.nc'

mani_fp_rock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/maySims/rock/folder_manifest.csv'

ds_ice, groupDsDict_ice = readDataset(fp_ice_rock, ['geotop'])
geotopDs_ice = groupDsDict_ice['geotop']

ds_tg, groupDsDict_tg = readDataset(fp_tg_rock, ['geotop'])
geotopDs_tg_rock = groupDsDict_tg['geotop']

ds_lw, groupDsDict_lw = readDataset(fp_liqWater_rock, ['geotop'])
geotopDs_lw_rock = groupDsDict_lw['geotop']

sitenames_rock = list(geotopDs_ice['sitename'][:])
simIDs_rock = list(geotopDs_ice['simulation'][:])
soil_depth_rock = list(geotopDs_ice['soil_depth'][:])
dates_rock = timeToDate(list(geotopDs_ice['Date'][:]))

siteSimDict_rock =  {'simID': simIDs_rock, 'site': sitenames_rock}
siteSimDf_rock = pd.DataFrame(siteSimDict_rock)

iceContentPerSim_geotop_rock = outputPerSim(geotopDs_ice, simIDs_rock,'ice_content')
tgPerSim_geotop_rock = outputPerSim(geotopDs_tg_rock, simIDs_rock,'Tg')
lwPerSim_rock = outputPerSim(geotopDs_lw_rock, simIDs_rock, 'liq_water')

simPerSite_rock = {}
for s in sitenames_rock:
    simPerSite_rock[s] = list(siteSimDf_rock[siteSimDf_rock['site']==s]['simID'])

plotOutputPerSite(iceContentPerSim_geotop_rock, simPerSite_rock['NGO-DD-2012'], soil_depth_rock, dates_rock,manifest_fp=mani_fp_rock,yvar='Ice content')
plotOutputPerSite(tgPerSim_geotop_rock, simPerSite_rock['NGO-DD-2012'], soil_depth_rock, dates_rock,yvar='Ground Temperature [C]',manifest_fp=mani_fp_rock)
plotOutputPerSite(lwPerSim_rock,simPerSite_rock['NGO-DD-2012'], soil_depth_rock, dates_rock,yvar='Liquid water content',manifest_fp=mani_fp_rock)


#%%% KDI Rock
fp_ice_rock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem/result_ice_content.nc'
fp_tg_rock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem/result_soil_temperature.nc'
fp_liqWater_rock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem/result_liquid_water.nc'

mani_fp_rock = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem/folder_manifest.csv'

ds_ice, groupDsDict_ice = readDataset(fp_ice_rock, ['geotop'])
geotopDs_ice = groupDsDict_ice['geotop']

ds_tg, groupDsDict_tg = readDataset(fp_tg_rock, ['geotop'])
geotopDs_tg_rock = groupDsDict_tg['geotop']

ds_lw, groupDsDict_lw = readDataset(fp_liqWater_rock, ['geotop'])
geotopDs_lw_rock = groupDsDict_lw['geotop']

sitenames_rock = list(geotopDs_ice['sitename'][:])
simIDs_rock = list(geotopDs_ice['simulation'][:])
soil_depth_rock = list(geotopDs_ice['soil_depth'][:])
dates_rock = timeToDate(list(geotopDs_ice['Date'][:]))

siteSimDict_rock =  {'simID': simIDs_rock, 'site': sitenames_rock}
siteSimDf_rock = pd.DataFrame(siteSimDict_rock)

iceContentPerSim_geotop_rock = outputPerSim(geotopDs_ice, simIDs_rock,'ice_content')
tgPerSim_geotop_rock = outputPerSim(geotopDs_tg_rock, simIDs_rock,'Tg')
lwPerSim_rock = outputPerSim(geotopDs_lw_rock, simIDs_rock, 'liq_water')

simPerSite_rock = {}
for s in sitenames_rock:
    simPerSite_rock[s] = list(siteSimDf_rock[siteSimDf_rock['site']==s]['simID'])

plotOutputPerSite(iceContentPerSim_geotop_rock, simPerSite_rock['KDI-E-Tslp'], soil_depth_rock, dates_rock,manifest_fp=mani_fp_rock,yvar='Ice content')
plotOutputPerSite(tgPerSim_geotop_rock, simPerSite_rock['KDI-E-Tslp'], soil_depth_rock, dates_rock,yvar='Ground Temperature [C]',manifest_fp=mani_fp_rock)
plotOutputPerSite(lwPerSim_rock,simPerSite_rock['KDI-E-Tslp'], soil_depth_rock, dates_rock,yvar='Liquid water content',manifest_fp=mani_fp_rock)
