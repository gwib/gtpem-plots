#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 10:46:51 2022

@author: GalinaJonat
"""
import xarray as xr
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

nc_monthly_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220621/resampled/tg_monthly.nc'
kdi_geotop_Tg_monthly = xr.open_dataset(nc_monthly_fp)

test_site = kdi_geotop_Tg_monthly.sitename.values[0,0]
test_kdi_Ds = kdi_geotop_Tg_monthly.where((kdi_geotop_Tg_monthly.sitename == test_site),drop=True).drop('pointid')

d = np.datetime64('1991')
kdi_geotop_Tg_1980_1990_site = test_kdi_Ds.where((test_kdi_Ds.Date < d), drop = True)
site_Tg_1980_1990_climatology = kdi_geotop_Tg_1980_1990_site.groupby('Date.month').mean(dim=['Date','simulation'])

site_anomaly = test_kdi_Ds.groupby('Date.month') - site_Tg_1980_1990_climatology

site_anomaly_min = site_anomaly.min(dim='simulation')
site_anomaly_max = site_anomaly.max(dim='simulation')
site_anomaly_mean = site_anomaly.mean(dim='simulation')

site_anomaly_sd = site_anomaly.std(dim='simulation')


x = np.array(site_anomaly.Date.values)
y = -np.array(site_anomaly.soil_depth.values)

tg_sd = np.array(site_anomaly_sd.Tg.values).transpose()
tg_mean = np.array(site_anomaly_mean.Tg.values).transpose()

fig2,ax2 = plt.subplots()

c2 = ax2.contourf(x,y,tg_mean,cmap=plt.cm.bwr)
fig2.colorbar(c2)
ax2.set_title('Monthly mean ground temperature anomaly [K]')
ax2.set_ylabel('Depth [m]')
ax2.set_xlabel('Year')


fig3,ax3 = plt.subplots()
c3 = ax3.contourf(x,y,tg_sd,cmap=plt.cm.gray_r)
fig3.colorbar(c3)
ax3.set_title('Ensemble standard deviation')
ax3.set_ylabel('Depth [m]')
ax3.set_xlabel('Year')

nc_annual_fp = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/gtpem_ens_20220621/resampled/soil_tg_yearly.nc'
kdi_geotop_Tg_annual = xr.open_dataset(nc_annual_fp)

test_site = kdi_geotop_Tg_monthly.sitename.values[0,0]
test_kdi_Ds = kdi_geotop_Tg_annual.where((kdi_geotop_Tg_annual.sitename == test_site),drop=True).drop('pointid')

d = np.datetime64('1991')
kdi_geotop_Tg_1980_1990_site = test_kdi_Ds.where((test_kdi_Ds.Date < d), drop = True)
site_Tg_1980_1990_climatology = kdi_geotop_Tg_1980_1990_site.groupby('Date.month').mean(dim=['Date','simulation'])

site_anomaly = test_kdi_Ds.groupby('Date.month') - site_Tg_1980_1990_climatology

site_anomaly_min = site_anomaly.min(dim='simulation')
site_anomaly_max = site_anomaly.max(dim='simulation')
site_anomaly_mean = site_anomaly.mean(dim='simulation')

site_anomaly_sd = site_anomaly.std(dim='simulation')


x = np.array(site_anomaly.Date.values)
y = -np.array(site_anomaly.soil_depth.values)

tg_sd = np.array(site_anomaly_sd.Tg.values).transpose()
tg_mean = np.array(site_anomaly_mean.Tg.values).transpose()

fig2,ax2 = plt.subplots()

c2 = ax2.contourf(x,y,tg_mean,cmap=plt.cm.bwr)
fig2.colorbar(c2)
ax2.set_title('Annual mean ground temperature anomaly [K]')
ax2.set_ylabel('Depth [m]')
ax2.set_xlabel('Year')


fig3,ax3 = plt.subplots()
c3 = ax3.contourf(x,y,tg_sd,cmap=plt.cm.gray_r)
fig3.colorbar(c3)
ax3.set_title('Ensemble standard deviation')
ax3.set_ylabel('Depth [m]')
ax3.set_xlabel('Year')