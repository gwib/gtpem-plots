#%%imports and basics
from matplotlib import pyplot as plt
import xarray as xr
import numpy as np
from timeSeriesEns import timeSeriesForSite
import seaborn as sns
sns.set_theme(style='darkgrid')

# System specs
from sys import platform
folder_talik = '/home/gjo713/storage/gtpem_output/'
folder_os = '/Users/GalinaJonat/OneDrive - Carleton University/niagara/kdi/'
sim_folder = 'kdi_ens_20220812/'
soil_temp_fn = '/resampled/soil_temperature_annualMean.nc'
ice_content_fn = '/resampled/ice_content_annualMin_integrated.nc'
tdd_fn = '/resampled/tdd.nc'
alt_fn = '/resampled/result_ALT2.nc'

if platform=='linux':
    folder = folder_talik+sim_folder
    plotPath = '/home/gjo713/storage/plots/'
elif platform=='darwin':
    folder = folder_os+sim_folder


#%% TDD and integrated ice content
tdd_Ds = xr.open_dataset(folder+tdd_fn)
iceContent_Ds = xr.open_dataset(folder+ice_content_fn)


# select data for site
site = 'KDI-W-Org2'
#test_iceContent = iceContent.where((iceContent.sitename == s1),drop=True).drop_vars(['sitename'])
site_tdd = tdd_Ds.where((tdd_Ds.sitename == site), drop=True).drop_vars(['sitename'])

site_iceContent = iceContent_Ds.where((iceContent_Ds.sitename == site), drop=True).drop_vars(['sitename'])



# # %% Calculate mean and quantiles
# qt_values = (0.25,0.75)

# tdd_mean_ds = site_tdd.mean(dim='simulation')
# tdd_quantiles = site_tdd.quantile(q=qt_values,dim='simulation')
# tdd_mean = tdd_mean_ds['tdd'].values
# tdd_lowerquantile = np.array(tdd_quantiles.sel(quantile = qt_values[0])['tdd'].values)
# tdd_upperQuantile = np.array(tdd_quantiles.sel(quantile = qt_values[1])['tdd'].values)

# iceContent_mean_ds = site_iceContent.mean(dim='simulation')
# iceContent_quantiles = site_iceContent.quantile(q=qt_values,dim='simulation')
# iceContent_mean = iceContent_mean_ds['totalIceContent'].values
# iceContent_lowerquantile = np.array(iceContent_quantiles.sel(quantile = qt_values[0])['totalIceContent'].values)
# iceContent_upperQuantile = np.array(iceContent_quantiles.sel(quantile = qt_values[1])['totalIceContent'].values)

# date = site_iceContent.Date.values

# fig = plt.figure()


# ax = fig.add_subplot()
# ax.plot(date,tdd_mean,label='TDD')    
# ax.fill_between(date,tdd_lowerquantile,tdd_upperQuantile,alpha=0.5,label=f'{qt_values} quantiles')
            
# ax.set_title(f'Ensemble time series')
# ax.set_ylabel(tdd_Ds.description)

# ax2 = ax.twinx()
# ax2.plot(date,iceContent_mean,label='Ice content',c='green')    
# ax2.fill_between(date,iceContent_lowerquantile,iceContent_upperQuantile,alpha=0.3,label=f'{qt_values} quantiles',color='green')
# ax2.set_ylabel(iceContent_Ds.description)

# handles, labels = ax.get_legend_handles_labels()
# fig.legend(handles, labels, loc='upper center')    
# fig.tight_layout()


#%%% same as seaborn plot
site_tdd_df = site_tdd.to_dataframe()
site_iceContent_df = site_iceContent.to_dataframe()
fig1, ax1 = plt.subplots()
ax2 = ax1.twinx()
sns.lineplot(data=site_tdd_df,x='Date',y='tdd',ax=ax2,color='#f3700e')
sns.lineplot(data=site_iceContent_df,x='Date',y='totalIceContent',ax=ax1,color="#1ce1ce")
ax1.set_ylabel('Minimum annual ice content')
ax2.set_ylabel('Thaw depth duration')
ax1.yaxis.label.set_color('#1ce1ce')
ax2.yaxis.label.set_color('#f3700e')
ax1.set_xlabel('Year')
fig1.savefig(plotPath+'iceContentTdd.png',dpi=300.0)


# %% ground temperature and ALT

tg_Ds = xr.open_dataset(folder+soil_temp_fn)
alt_Ds = xr.open_dataset(folder+alt_fn)

site_tg = tg_Ds.where((tdd_Ds.sitename == site), drop=True).drop_vars(['sitename','model','pointid'])
site_alt = alt_Ds.where((tdd_Ds.sitename == site), drop=True).drop_vars(['sitename','model','pointid'])

site_tg_mean = site_tg.mean(dim='simulation')

site_tg_sd = site_tg.std(dim='simulation')

site_alt_mean = site_alt.mean(dim='simulation')
alt_values = np.array(site_alt_mean.AL.values)

x = np.array(site_tg.Date.values)
y = -np.array(site_tg.soil_depth.values)

tg_sd = np.array(site_tg_sd.Tg.values).transpose()
tg_mean = np.array(site_tg_mean.Tg.values).transpose()

fig7,ax7 = plt.subplots()
c7 = ax7.contourf(x,y,tg_mean,cmap=plt.cm.PuBu_r)
fig7.colorbar(c7)
#ax7.set_title('Annual mean ground temperatures [C] and Active Layer Thickness')
ax7.set_ylabel('Depth [m]')
ax7.set_xlabel('Year')

fig6,ax6 = plt.subplots()
c6 = ax6.contourf(x,y,tg_sd,cmap=plt.cm.gray_r)
fig6.colorbar(c6)
ax6.set_title('Ensemble standard deviation')
ax6.set_ylabel('Depth [m]')
ax6.set_xlabel('Year')

site_alt_mean = site_alt.mean(dim='simulation').AL.values
site_alt_sd = site_alt.std(dim='simulation').AL.values
ax7.plot(x,-site_alt_mean,c='black',label='ALT')
fig7.savefig(plotPath+'soilTempALT.png',dpi=300.0)
